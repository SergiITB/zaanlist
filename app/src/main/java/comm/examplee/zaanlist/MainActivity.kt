package comm.examplee.zaanlist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import zaanlist.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}