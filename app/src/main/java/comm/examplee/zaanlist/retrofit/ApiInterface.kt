package comm.examplee.zaanlist.retrofit


import comm.examplee.zaanlist.models.Data
import comm.examplee.zaanlist.models.DataItem
import comm.examplee.zaanlist.models.Lista
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @GET()
    suspend fun getDataListas(@Url url: String): Response<MutableList<Lista>>

    @GET()
    suspend fun getDataItems(@Url url: String): Response<Data>

    @POST("todolists")
    suspend fun addLista(@Body lista: Lista): Response<Lista>

    @POST("todoitems")
    suspend fun addItem(@Body item: DataItem): Response<DataItem>

    @DELETE("todolists/{idLista}")
    suspend fun deleteList(@Path("idLista") idLista: Int): Response<Lista>

    @DELETE("todoitems/{id}")
    suspend fun deleteItem(@Path("id") id: Int): Response<DataItem>

    @PUT("todoitems")
    suspend fun checkItem(@Body item: DataItem): Response<DataItem>

    @PUT("todoitems")
    suspend fun editItem(@Body item: DataItem): Response<DataItem>

    @PUT("todolists")
    suspend fun editLista(@Body lista: Lista): Response<Lista>

    companion object {
        private const val BASE_URL = "https://zaanlist.herokuapp.com/zaan/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}