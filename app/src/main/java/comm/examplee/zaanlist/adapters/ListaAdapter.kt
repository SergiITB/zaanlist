package comm.examplee.zaanlist.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import comm.examplee.zaanlist.Repository
import comm.examplee.zaanlist.models.Data
import comm.examplee.zaanlist.models.DataItem
import comm.examplee.zaanlist.models.Lista
import comm.examplee.zaanlist.models.OnClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import zaanlist.R
import zaanlist.databinding.ItemListaBinding
import zaanlist.databinding.ItemListaListaBinding

class ListaAdapter(private val listas: MutableList<Lista>, private val listener: OnClickListener) :
    RecyclerView.Adapter<ListaAdapter.ViewHolder>() {

    private lateinit var context: Context
    private var layoutType = 1

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding =
            if (layoutType == 0) ItemListaListaBinding.bind(view) else ItemListaBinding.bind(view)

        fun setListener(lista: Lista) {
            binding.root.setOnClickListener {
                listener.onClick(lista)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutId: Int = if (layoutType == 0) {
            R.layout.item_lista_lista
        } else {
            R.layout.item_lista
        }
        val view = LayoutInflater.from(context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lista = listas[position]
        with(holder) {
            setListener(listas[position])
            if (binding is ItemListaListaBinding) {
                binding.listName.text = lista.listaName

                binding.removeIcon.setOnClickListener {
                    listener.onClickRemove(lista)
                }
                binding.editIcon.setOnClickListener {
                    listener.onClickEdit(lista)
                }
            } else {
                (binding as ItemListaBinding)
                binding.listName.text = lista.listaName

                binding.removeIcon.setOnClickListener {
                    listener.onClickRemove(lista)
                }
                binding.editIcon.setOnClickListener {
                    listener.onClickEdit(lista)
                }

                val repository = Repository()
                val getItems = MutableLiveData<Data>()
                val items = mutableListOf<DataItem>()
                CoroutineScope(Dispatchers.Main).launch {
                    val itemsRep: Response<Data> =
                        withContext(Dispatchers.IO) { repository.fetchItems("todoitems") }
                    print(itemsRep)
                    if (itemsRep.isSuccessful) {
                        getItems.value = itemsRep.body()
                        for (item in getItems.value!!) {
                            if (item.lista.idLista == lista.idLista) {
                                items.add(item)
                            }
                        }
                        when {
                            items.size > 3 -> {
                                binding.item1.text = items[0].itemName
                                binding.item2.text = items[1].itemName
                                binding.item3.text = items[2].itemName
                            }
                            items.size == 3 -> {
                                binding.item1.text = items[0].itemName
                                binding.item2.text = items[1].itemName
                                binding.item3.text = items[2].itemName
                                binding.textEnd.text = ""
                            }
                            items.size == 2 -> {
                                binding.item1.text = items[0].itemName
                                binding.item2.text = items[1].itemName
                                binding.item3.text = ""
                                binding.textEnd.text = ""
                            }
                            items.size == 1 -> {
                                binding.item1.text = items[0].itemName
                                binding.item2.text = ""
                                binding.item3.text = ""
                                binding.textEnd.text = ""
                            }
                            items.isEmpty() -> {
                                binding.item1.text = "LISTA"
                                binding.item2.text = "VACIA"
                                binding.item3.text = ""
                                binding.textEnd.text = ""
                            }
                        }
                    } else {
                        Log.e("Error :", itemsRep.message())
                    }
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return listas.size
    }

    fun setLayoutType(type: Int) {
        layoutType = type
    }

}


