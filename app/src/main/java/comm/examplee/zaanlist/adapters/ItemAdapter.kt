package comm.examplee.zaanlist.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import comm.examplee.zaanlist.models.Data
import comm.examplee.zaanlist.models.DataItem
import comm.examplee.zaanlist.models.OnClickListenerList
import zaanlist.R
import zaanlist.databinding.ItemObjectBinding

class ItemAdapter(private val items: Data, private val listener: OnClickListenerList) :
    RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemObjectBinding.bind(view)
        fun setListener(item: DataItem) {
            binding.root.setOnClickListener {
                listener.onClick(item)
            }
            binding.removeIcon.setOnClickListener {
                listener.onClickRemove(item)
            }
            binding.editIcon.setOnClickListener {
                listener.onClickEdit(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutId = R.layout.item_object
        val view = LayoutInflater.from(context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        with(holder) {
            if (item.hecho) {
                setListener(items[position])
                binding.itemName.text = item.itemName
                binding.item.setBackgroundResource(R.drawable.border_checked)
                binding.editIcon.visibility = View.INVISIBLE
            }
            setListener(items[position])
            binding.itemName.text = item.itemName
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}