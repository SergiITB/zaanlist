package comm.examplee.zaanlist.models

data class DataItem(
    var hecho: Boolean,
    val id: Int,
    var itemName: String,
    var lista: Lista
)