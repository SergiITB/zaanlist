package comm.examplee.zaanlist.models

interface OnClickListenerList {
    fun onClick(item: DataItem)
    fun onClickRemove(item: DataItem)
    fun onClickEdit(item: DataItem)
}