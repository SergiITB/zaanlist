package comm.examplee.zaanlist.models

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import comm.examplee.zaanlist.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ViewModel : ViewModel() {
    private var repository = Repository()

    var items = MutableLiveData<Data?>()
    var lists = MutableLiveData<MutableList<Lista>>()

    var actualList = MutableLiveData<Lista>()

    init {
        fetchLists("todolists")
    }

    fun fetchLists(url: String) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.fetchLists(url) }
            if (response.isSuccessful) {
                lists.postValue(response.body())
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    private fun fetchItems(url: String) {
        items.value = Data()
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.fetchItems(url) }
            if (response.isSuccessful) {
                items.postValue(response.body())
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun listItems(list: Lista) {
        actualList.value = list
        fetchItems("todolists/" + actualList.value!!.idLista + "/todoitems")
    }

    fun addList(lista: Lista) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addList(lista) }
            if (response.isSuccessful) {
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun addItem(item: DataItem) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addItem(item) }
            if (response.isSuccessful) {
                listItems(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun deleteList(lista: Lista) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.deleteList(lista) }
            if (response.isSuccessful) {
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun deleteItem(item: DataItem) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.deleteItem(item) }
            if (response.isSuccessful) {
                listItems(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun checkItem(item: DataItem) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.checkItem(item) }
            if (response.isSuccessful) {
                listItems(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun editItem(item: DataItem) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.editItem(item) }
            if (response.isSuccessful) {
                listItems(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun editLista(lista: Lista) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.editList(lista) }
            if (response.isSuccessful) {
                for (item in items.value!!){
                    addItem(item)
                }
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }
}