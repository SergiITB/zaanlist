package comm.examplee.zaanlist.models

interface OnClickListener {
    fun onClick(lista: Lista)
    fun onClickEdit(lista: Lista)
    fun onClickRemove(lista: Lista)
}