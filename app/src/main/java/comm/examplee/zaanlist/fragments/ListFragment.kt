package comm.examplee.zaanlist.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import comm.examplee.zaanlist.adapters.ItemAdapter
import comm.examplee.zaanlist.models.*
import zaanlist.R
import zaanlist.databinding.FragmentListBinding


class ListFragment : Fragment(), OnClickListenerList {
    private lateinit var itemAdapter: ItemAdapter
    private lateinit var binding: FragmentListBinding
    private val model: ViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.items.value?.let {
            setUpRecyclerView(it)
        }

        binding.listName.text = model.actualList.value!!.listaName

        model.items.observe(viewLifecycleOwner, Observer {
            if (model.items.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                if (it != null) {
                    setUpRecyclerView(it)
                }
            }
        })

        binding.sendIcon.setOnClickListener {
            val text = binding.inputNewItemText.text.toString()
            if (text != "") {
                newItem(text)
            }
            hideKeyboard(view)
            binding.inputNewItemText.text = null
        }
    }

    private fun newItem(text: String) {
        val lista: Lista = model.actualList.value!!
        val newItem = DataItem(false, 0, text, lista)
        model.addItem(newItem)
        model.listItems(lista)
    }

    private fun setUpRecyclerView(myData: Data) {
        itemAdapter = ItemAdapter(myData, this)
        binding.itemRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = itemAdapter
        }
    }

    override fun onClick(item: DataItem) {
        item.hecho = !item.hecho
        model.checkItem(item)
    }

    override fun onClickRemove(item: DataItem) {
        model.deleteItem(item)
    }

    @SuppressLint("SetTextI18n")
    override fun onClickEdit(item: DataItem) {
        val dialogue = Dialog(this.requireContext())
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogue.setContentView(R.layout.edit)
        val editTypeText = dialogue.findViewById<TextView>(R.id.editTypeText)
        val inputEditText = dialogue.findViewById<TextInputEditText>(R.id.inputEditListText)
        val sendIcon = dialogue.findViewById<ImageView>(R.id.send_icon)
        editTypeText.text = "EDITAR NOMBRE ITEM"
        inputEditText.hint = "Nombre antiguo: ${item.itemName}"
        sendIcon.setOnClickListener {
            if (inputEditText.text.toString() != "") {
                item.itemName = inputEditText.text.toString()
                model.editItem(item)
            }
            hideKeyboard(it)
            inputEditText.text = null
            dialogue.dismiss()
        }
        dialogue.show()
    }

    private fun hideKeyboard(view: View) {
        view.apply {
            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}