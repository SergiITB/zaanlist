package comm.examplee.zaanlist.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import comm.examplee.zaanlist.adapters.ListaAdapter
import comm.examplee.zaanlist.models.Lista
import comm.examplee.zaanlist.models.OnClickListener
import comm.examplee.zaanlist.models.ViewModel
import zaanlist.R
import zaanlist.databinding.FragmentListsBinding


class ListsFragment : Fragment(), OnClickListener {
    private lateinit var listaAdapter: ListaAdapter
    private lateinit var binding: FragmentListsBinding
    private var layoutType = 1
    private val model: ViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentListsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.lists.value?.let {
            setUpRecyclerView(it, layoutType)
        }

        model.lists.observe(viewLifecycleOwner, Observer {
            if (model.lists.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it, layoutType)
            }
        })

        binding.listIcon.setOnClickListener {
            if (layoutType == 0) {
                layoutType = 1
                binding.listIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.list_icon_lists
                    )
                )
            } else {
                layoutType = 0
                binding.listIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.grid_icon_lists
                    )
                )
            }
            setUpRecyclerView(model.lists.value!!, layoutType)
        }

        binding.addIcon.setOnClickListener {
            if (binding.inputNewListText.visibility == View.VISIBLE) {
                binding.inputNewListText.visibility = View.INVISIBLE
                binding.sendIcon.visibility = View.INVISIBLE
                binding.view.visibility = View.INVISIBLE

            } else {
                binding.inputNewListText.setText("")
                binding.inputNewListText.visibility = View.VISIBLE
                binding.sendIcon.visibility = View.VISIBLE
                binding.view.visibility = View.VISIBLE
            }
            hideKeyboard(it)
        }
        binding.sendIcon.setOnClickListener {
            val text = binding.inputNewListText.text.toString()
            newList(text, it)
            model.lists.value?.let { itList ->
                model.fetchLists("todolists")
                setUpRecyclerView(itList, layoutType)
            }
        }
    }

    private fun newList(text: String, view: View) {
        if (text != "") {
            val newList = Lista(0, text)
            model.addList(newList)
        }
        hideKeyboard(view)
        binding.inputNewListText.visibility = View.INVISIBLE
        binding.sendIcon.visibility = View.INVISIBLE
        binding.view.visibility = View.INVISIBLE
    }

    private fun setUpRecyclerView(myData: MutableList<Lista>, layoutType: Int) {
        listaAdapter = ListaAdapter(myData, this)
        listaAdapter.setLayoutType(layoutType)
        binding.listaRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager =
                if (layoutType == 0) LinearLayoutManager(context) else GridLayoutManager(context, 2)
            adapter = listaAdapter
        }
    }

    override fun onClick(lista: Lista) {
        model.listItems(lista)
        findNavController().navigate(R.id.action_listsFragment_to_listFragment)
    }

    override fun onClickRemove(lista: Lista) {
        model.deleteList(lista)
    }

    @SuppressLint("SetTextI18n")
    override fun onClickEdit(lista: Lista) {
        model.listItems(lista)
        val dialogue = Dialog(this.requireContext())
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogue.setContentView(R.layout.edit)
        val editTypeText = dialogue.findViewById<TextView>(R.id.editTypeText)
        val inputEditText = dialogue.findViewById<TextInputEditText>(R.id.inputEditListText)
        val sendIcon = dialogue.findViewById<ImageView>(R.id.send_icon)
        editTypeText.text = "EDITAR NOMBRE LISTA"
        inputEditText.hint = "Nombre antiguo: ${lista.listaName}"
        sendIcon.setOnClickListener {
            if (inputEditText.text.toString() != "") {
                lista.listaName = inputEditText.text.toString()
                model.editLista(lista)

            }
            hideKeyboard(it)
            inputEditText.text = null
            dialogue.dismiss()
        }
        dialogue.show()
    }

    private fun hideKeyboard(view: View) {
        view.apply {
            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}