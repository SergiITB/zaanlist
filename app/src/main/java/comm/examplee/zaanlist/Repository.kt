package comm.examplee.zaanlist

import comm.examplee.zaanlist.models.DataItem
import comm.examplee.zaanlist.models.Lista
import comm.examplee.zaanlist.retrofit.ApiInterface

class Repository {
    private val apiInterface = ApiInterface.create()

    suspend fun fetchLists(url: String)  = apiInterface.getDataListas(url)

    suspend fun fetchItems(url: String)  = apiInterface.getDataItems(url)

    suspend fun addList(lista: Lista)  = apiInterface.addLista(lista)

    suspend fun addItem(item: DataItem)  = apiInterface.addItem(item)

    suspend fun deleteList(lista: Lista)  = apiInterface.deleteList(lista.idLista)

    suspend fun deleteItem(item: DataItem)  = apiInterface.deleteItem(item.id)

    suspend fun checkItem(item: DataItem)  = apiInterface.checkItem(item)

    suspend fun editList(lista: Lista)  = apiInterface.editLista(lista)

    suspend fun editItem(item: DataItem)  = apiInterface.editItem(item)

}